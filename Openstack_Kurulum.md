### Openstack Kurulum

* stack adında bir kullanıcı eklenir.

```bash
sudo useradd -s /bin/bash -d /opt/stack -m stack
echo "stack ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/stack
```

* stack kullanıcısına geçilir.

```bash
sudo su - stack
```

* git clone ile ilgili dosya çekilir.

```bash
git clone https://opendev.org/openstack/devstack
```

* devstack dizinine geçilir ve sonrasında local.conf dosyası düzenlenir. local.conf dosyası `samples` subfolderinde olduğundan dolayı `devstack` dizini altına alınmalıdır.
* Her ihtimale karşı local.conf dosyasının yedeği alınır.

```bash
cd devstack
cp samples/local.conf .
mv samples/local.conf samples/local.conf.bck

```

* local.conf dosyasındaki aşağıda gösterilen alanlar bu şekilde düzenlenir.

```bash
[[local|localr]]
 
ADMIN_PASSWORD=istenen_parola
DATABASE_PASSWORD=$ADMIN_PASSWORD
RABBIT_PASSWORD=$ADMIN_PASSWORD
SERVICE_PASSWORD=$ADMIN_PASSWORD
```

* devstack içerisindeki stack.sh betiği çalıştırılır.

```bash
./stack.sh
```

* Kurulum esnasında hata vermesi muhtemel. Alınan hataya göre araştırma yapılabilir ancak sık karşılaşılan hatalardan biri betiğin çalışmasından bir süre sonra openstack github hesabından veri çekmemesi olabilir.
  * Bunun çözümü şu şekilde sağlanır: `devstack/stackrc` dosyası içerisindeki `GIT_BASE=....https://opendev.org` kısmı yorum satırına alınıp dosya içeriğine `GIT_BASE=${GIT_BASE: -https://git.openstack.org}` satırı eklenir.

* Hata alınan bir nokta da python3 ile birkaç dosya yüklerken oluşan hatalardı. Bunlar da 

  ```bash
  sudo pip install --upgrade pip setuptools
  sudo pip install sqlalchemy
  ```

   

  kullanılarak çözüldü. setuptools'un güncel versiyonunu almadığından dolayı kuruluma devam etmedi, güncelledikten sonra kurulum tamamlandı. sqlalchemy zaten makinede olduğundan dolayı yükleme yapmadı. 