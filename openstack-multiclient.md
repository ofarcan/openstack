### Openstack Kurulum

#### Controller Node

- İşlerin yürütüldüğü master node olarak düşünülebilir.
- Bu node kurulduktan sonra host olarak görünür ve sonrasında kurulacak Compute, Object vb node'ların konfigürasyon dosyalarında bu node host olarak seçilip kendisine bağlanır.

Kurulum devstack ile yapıldı. https://docs.openstack.org/devstack/latest/guides/multinode-lab.html sayfasındaki yönergeler kullanıldı.

* Test etmek için işlemler fiziksel ortamda yapılmadan önce sanal ortamda yapıldı. 

  * Controller için bir Ubuntu20-Server sanal makine oluşturuldu.

  * Bu işlem için Virtualbox sanallaştırma ortamı kullanıldı.

  * VM oluşturulup kurulum yapıldıktan sonra `/etc/netplan/...yaml` dosyasını düzenleyerek makineye bir statik ip eklemesi yapılır.

  * Makineye erişim yapıldıktan sona python3'ün güncel versiyonu, git ve pip yüklenmesi gerekli.

    ​	

    ```bash
    sudo apt-get install git python3 pip -y
    ```

  * Stack adında bir user oluşturulur. Password istemeden user'a geçebilmesi için aşağıdaki gibi işlem yapılır.

    ```bash
    useradd -s /bin/bash -d /opt/stack -m stack
    chmod +x /opt/stack
    echo "stack ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/stack
    ```

    

  * Sanal ortamda hem makineleri haberleştirmek, hem local ortamdan erişimlerinin sağlanması hem de makinelerin internete erişmelerini sağlamak gerekli idi.
  
    * Bunu sağlamak için makinelerin Network ayarları kısmında 3 farklı Network Adapter kullanıldı.
  
      * Adapter1 = Bridged Adapter
        * Bu adapter sayesinde makineler localde bağlı olan makineye erişebilirler. Local makineden bu sanal makinelere SSH erişimi sağlanabilir. Aynı zamanda internete erişmeye de olanak sağlar ancak internete erişim için farklı bir adapter daha kullanıldı.
      * Adapter2 = NAT Network
        * Bu adapteri doğru yapılandırmak için *File > Preferences > Network* kısmından NAT Network eklendi. Kullanılan sanal makinelerin ip'leri 10.1.204.200, 201, 202 olduğu için kullanılan NAT Network CIDR değeri *10.1.204.0/24* olarak belirlendi.
        * Kullanılan bu adapter sayesinde makinelerin birbirlerine erişimleri mümkün kılındı. 
          * Sonuç olarak Controller Node üzerinden diğer eklenen Node'lar kontrol edileceği için makinelerin haberleşmesi gerekiyor. Bu adapter haberleşme işlemleri konusunda yardımcı oluyor. 
      * Adapter3 = NAT
        * Bu adapter makinelerin internete erişmeleri konusunda işlem yapar. Aslında Bridged Mode kullanıldığında local makinenin internete erişmesi mümkün olduğunda sanal makineler de erişim sağlayabiliyor. Ancak yapılan işlemlerde *git clone* işlemlerinde bazen düşmeler yaşanabiliyor Bridged Mode kullanıldığında. NAT Adapter kullanıldığında bu kısmın önüne geçilebiliyor.
  
      ***Fiziksel orama kurulum yapılırken bu şekilde bir yapılandırmaya gerek duyulmayacak.***
  
      
  
    * Bunun haricinde mevcutta çalışılan ağın içerisinde boş ip'ler elde edililrse Bridge ile kendi ağınızda bulunan bir ip'ye atama da yapılabilir.
      * Ör: Kendi ağınız içerisinde aldığınız ip 10.1.X.Y olacaksa makinelere statik olarak 10.1.X.150 ve 10.1.X.151 ipleri atanabilir.
      * **Bu kurulum örneği için controller ip'sinin 10.1.204.200, Compute ip'sinin 10.1.204.201 ve Object ip'sinin 10.1.204.202 olduğunu varsayıp dökümana bu şekilde devam edilecek.**
        * Bu ip'lerin farklı bir cihaz tarafından kullanılmadığından/kullanılamayacağından emin olmak gerekir.
  
  * Daha önceki denemelerde python3 ile alakalı hayaları alındığından dolayı öncelikle python3 kaldırıldı.

    ```bash
    apt-get remove --purge python3 -y
    ```
  
  * Sonrasında gerekli olan pip, python3, git, net-tools kurulumu yapılır.

    ```bash
    apt-get install git net-tools python3 pip -y
    ```

  * Stack kullanıcısına geçiş yapılır.
  
    ```bash
    sudo su - stack
    ```

  * Git HTTP POST işleminin arabellek boyutunu 1G yapmak için 

    ```bash
    git config --global http.postBuffer 1073741824
    ```
  
    komutu kullanılır. Openstack kurulumu için çalışan betik birçok `git clone` işlemi barındırır ve bazı dosyaların boyutu büyük. Default değerde bırakılırsa dosyaları çekmeme ihtimali mevcut.
  
  * Devstack `git clone` ile çekilir.
  
    ```bash
    git clone https://opendev.org/openstack/devstack
    ```
  
  * `/opt/stack/devstack/samples` içerisinde bulunan `local.sh ve local.conf` dosyaları `/opt/stack/devstack` dizinine kopyalanır.
  
    * Bu dosyaların içeriklerinde düzenlenmesi gereken alanlar mevcut. Ayrıca configuration dosyalarının devstack dizininde yer alması gerekir.
  
  * `local.conf` dosyası içeriğinde default olarak aşağıdaki gibi bir yapı mevcut.
  
    ```bash
    [[local|localrc]]
    
    # Minimal Contents
    # ----------------
    
    # While ``stack.sh`` is happy to run without ``localrc``, devlife is better when
    # there are a few minimal variables set:
    
    # If the ``*_PASSWORD`` variables are not set here you will be prompted to enter
    # values for them by ``stack.sh``and they will be added to ``local.conf``.
    ADMIN_PASSWORD=nomoresecret
    DATABASE_PASSWORD=stackdb
    RABBIT_PASSWORD=stackqueue
    SERVICE_PASSWORD=$ADMIN_PASSWORD
    ```

    * Burada birkaç düzenleme yapmak gerekir. Aşağıdaki gibi düzenleme yapılabilir.
  
      ```bash
      [[local|localrc]]
      HOST_IP=10.1.204.200
      FIXED_RANGE=10.4.128.0/20
      FLOATING_RANGE=10.1.204.128/25
      LOGFILE=/opt/stack/logs/stack.sh.log
      ADMIN_PASSWORD=belirleyeceginiz_sifre
      DATABASE_PASSWORD=$ADMIN_PASSWORD
      RABBIT_PASSWORD=$ADMIN_PASSWORD
      SERVICE_PASSWORD=$ADMIN_PASSWORD
      ```
  
  * local.sh dosyası içeriğine aşağıdaki gibi bir for loop eklenir.
  
    ```bash
    for i in `seq 2 10`; do /opt/stack/nova/bin/nova-manage fixed reserve 10.4.128.$i; done
    ```
  
    * Bu satır, Nova sunucusu için 10.4.128.2 ila 10.4.128.10 arasındaki IP adreslerini sabit olarak rezerve etmenizi sağlar. Bu, Nova'nın bu IP adreslerini kullanarak yeni sanal makine oluşturmasını sağlar. Bu IP adreslerinin sabit olarak rezerve edilmesi, yapılandırmanızın daha prensipli olmasını sağlar ve bu IP adreslerinin diğer işlemler tarafından kullanılmamasını sağlar.
  
  * `/opt/stack/devstack/stackrc` dosyası içeriğinde `GIT_BASE=${GIT_BASE:-https://opendev.org}` satırı mevcut ancak bazı çalışacak betik içerisinde bazı dosyaları opendev üzerinden alamadığından dolayı `GIT_BASE=${GIT_BASE:-https://git.openstack.org}` satırını da dosya içeriğine eklemek gerekiyor.



Sonrasında `./stack.sh` ile kurulum başlatılır. 30 dk kadar sürüyor betiğin tamamlanması.



Tamamlandıktan sonra `10.1.204.200/dashboard` ile Openstack arayüzüne ulaşım sağlanabilir. Diğer node'lar henüz eklenmediğinden dolayı arayüzde **Admin > Compute > Hypervisor** kısmını veya **Admin > Systerm > System Information** kısmında yalnızca Controller Node görünecektir.

#### Compute Node

* Controller için bir Ubuntu20-Server sanal makine oluşturuldu.

* Bu işlem için Virtualbox sanallaştırma ortamı kullanıldı.

* VM oluşturulup kurulum yapıldıktan sonra `/etc/netplan/...yaml` dosyasını düzenleyerek makineye bir statik ip eklemesi yapılır.

  * Statik ip olarak Controller Node içerisinde anlatıldığı gibi *10.1.204.201* ipsi verildi.

* Makineye erişim yapıldıktan sona python3'ün güncel versiyonu, git ve pip yüklenmesi gerekli.

  * Controller Node içerisinde yapıldığı gibi öncelikle python3'ü kaldırılıp ardından tekrar kuruldu.

* Controller node içerisinde iletildiği gibi ***stack*** kullanıcısı oluşturulup bu kullanıcıya geçilir.

* Ardından 

  ```bash
  git clone https://opendev.org/openstack/devstack
  ```

  ile devstack ortamı çekilir.

* Burada Controller Node içerisinde yapılan **local.sh** içerisine eklenen `for i in `seq 2 10`; do /opt/stack/nova/bin/nova-manage fixed reserve 10.4.128.$i; done` satırı ***eklenmez.***

* Yalnızca *local.conf* dosyası *devstack/samples/* dizini altından *devstack* dizinine kopyalanır.

* İçeriği aşağıdaki gibi düzenlenir. local.conf dosyasında default olarak gelen kısımlar (ADMIN_PASSWORD) alanları silinip yerine aşağıdaki gibi düzenleme yapılır. Dosyanın orijinal halinde alt tarafta bulunan *SWIFT_HASH* gibi alanlar mevcut. O kısımlar **silinmeyecek.** *localrc* altında bu düzenlemeler yapılır.

  ```bash
  [[local|localrc]]
  
  # Minimal Contents
  # ----------------
  
  HOST_IP=10.1.204.201 # change this per compute node
  FIXED_RANGE=10.4.128.0/20
  FLOATING_RANGE=10.1.204.128/25
  LOGFILE=/opt/stack/logs/stack.sh.log
  ADMIN_PASSWORD=balistika
  DATABASE_PASSWORD=$ADMIN_PASSWORD
  RABBIT_PASSWORD=$ADMIN_PASSWORD
  SERVICE_PASSWORD=$ADMIN_PASSWORD
  DATABASE_TYPE=mysql
  SERVICE_HOST=10.1.204.200
  MYSQL_HOST=$SERVICE_HOST
  RABBIT_HOST=$SERVICE_HOST
  GLANCE_HOSTPORT=$SERVICE_HOST:9292
  ENABLED_SERVICES=n-cpu,c-vol,placement-client,ovn-controller,ovs-vswitchd,ovsdb-server,q-ovn-metadata-agent
  NOVA_VNC_ENABLED=True
  NOVNCPROXY_URL="http://$SERVICE_HOST:6080/vnc_lite.html"
  VNCSERVER_LISTEN=$HOST_IP
  VNCSERVER_PROXYCLIENT_ADDRESS=$VNCSERVER_LISTEN
  
  ```

* stackrc dosyası içerisinde Controller Node içerisinde yapıldığı gibi `GIT_BASE=${GIT_BASE:-https://git.openstack.org}` satırı eklenir.

#### Object Node

* Bu kurulumda Compute Node kurulumunda yapılan işlemlerin aynıları yapılıyor.

* Yapılması gereken işlem local.conf dosyasındaki ip değerinin değiştirilmesi olacak.

  ```bash
  [[local|localrc]]
  
  # Minimal Contents
  # ----------------
  
  HOST_IP=10.1.204.202 # change this per compute node
  FIXED_RANGE=10.4.128.0/20
  FLOATING_RANGE=10.1.204.128/25
  LOGFILE=/opt/stack/logs/stack.sh.log
  ADMIN_PASSWORD=balistika
  DATABASE_PASSWORD=$ADMIN_PASSWORD
  RABBIT_PASSWORD=$ADMIN_PASSWORD
  SERVICE_PASSWORD=$ADMIN_PASSWORD
  DATABASE_TYPE=mysql
  SERVICE_HOST=10.1.204.200
  MYSQL_HOST=$SERVICE_HOST
  RABBIT_HOST=$SERVICE_HOST
  GLANCE_HOSTPORT=$SERVICE_HOST:9292
  ENABLED_SERVICES=n-cpu,c-vol,placement-client,ovn-controller,ovs-vswitchd,ovsdb-server,q-ovn-metadata-agent
  NOVA_VNC_ENABLED=True
  NOVNCPROXY_URL="http://$SERVICE_HOST:6080/vnc_lite.html"
  VNCSERVER_LISTEN=$HOST_IP
  VNCSERVER_PROXYCLIENT_ADDRESS=$VNCSERVER_LISTEN
  ```

* ***SERVICE_HOST=10.1.204.200*** değeri işlemleri asıy yürütecek olan Controller Node ip'si.



**Kurulumda Alınacak Muhtemel Hatalar**

***Nova-compute hatası***

```bash
+::                                        ID=
+::                                        [[ '' == '' ]]
+::                                        sleep 1
+::                                        [[ libvirt = \f\a\k\e ]]
++::                                        openstack --os-cloud devstack-admin --os-region RegionOne compute service list --host object --service nova-compute -c ID -f value
```

Nova-compute normalde stack.sh dosyası çalıştırıldığında kuruluyor ancak kurulumu yapılamadığında veya servis durduğunda bu hata ile karşılaşılması muhtemel. Servisin durumu kontrol edilir.

```bash
stack@object:~/devstack$ systemctl status nova-compute
Unit nova-compute.service could not be found.

```

* Servis yok ise kurulması gerekiyor. `sudo apt-get install nova-compute -y` ile kurulum yapılabilir.

* Servis var ancak çalışmıyor ise `systemctl restart nova-compute` ile yeniden başlatılabilir.

* nova-compute servisinin sağlıklı çalışmadığı zamanda veya bu konuda hata alınan durumlarda `openstack --os-cloud devstack-admin --os-region RegionOne compute service list` komutu kullanılarak node'lardaki nova-compute kontrolü aşağıdaki gibi sağlanabilir.

  ```
  openstack --os-cloud devstack-admin --os-region RegionOne compute service list
  
  ID                                   | Binary         | Host       | Zone     | Status  | State | Updated At                 
  90645c99-f2fa-4799-934f-f4af2e287976 | nova-scheduler | controller | internal | enabled | down  | 2023-01-27T05:59:41.000000 |
  3cfe8671-9ae7-4603-a703-0c85bb7e0629 | nova-conductor | controller | internal | enabled | down  | 2023-01-27T05:59:42.000000 |
  00401414-7352-42a7-8f4f-61da46f320d1 | nova-conductor | controller | internal | enabled | down  | 2023-01-27T05:59:41.000000 |
  3cc551d8-7497-4193-9aa6-2421072980ba | nova-compute   | controller | nova     | enabled | down  | 2023-01-27T05:59:41.000000 |
  d1f7c3ec-547d-4cf1-b83e-357c3c0f6a13 | nova-compute   | compute    | nova     | enabled | down  | 2023-01-27T05:59:39.000000 |
  
  
  ```

* Sağlıklı çalıştığı zamanda görünüm aşağıdaki gibi olacaktır. State durumlarına bakıldığında **up** durumda olduğu görünüyor. Status durumları da sorun olduğunda **disabled** duruma düşebiliyor.

  ```bash
  stack@object:~/devstack$ openstack --os-cloud devstack-admin --os-region RegionOne compute service list
  
  | ID                                   | Binary         | Host       | Zone     | Status  | State | Updated At             
  
  | 90645c99-f2fa-4799-934f-f4af2e287976 | nova-scheduler | controller | internal | enabled | up    | 2023-01-30T13:55:45.000000 |
  | 3cfe8671-9ae7-4603-a703-0c85bb7e0629 | nova-conductor | controller | internal | enabled | up    | 2023-01-30T13:55:46.000000 |
  | 00401414-7352-42a7-8f4f-61da46f320d1 | nova-conductor | controller | internal | enabled | up    | 2023-01-30T13:55:45.000000 |
  | 3cc551d8-7497-4193-9aa6-2421072980ba | nova-compute   | controller | nova     | enabled | up    | 2023-01-30T13:55:51.000000 |
  | d1f7c3ec-547d-4cf1-b83e-357c3c0f6a13 | nova-compute   | compute    | nova     | enabled | up    | 2023-01-30T13:55:44.000000 |
  | 54494739-dec3-49c0-9652-88b33a4c0458 | nova-compute   | object     | nova     | enabled | up    | 2023-01-30T13:55:45.000000 |
  
  
  ```

***RPC Failed Hatası***

```bash
error: RPC failed; curl 56 GnuTLS recv error (-9): Error decoding the received TLS packet.
fatal: the remote end hung up unexpectedly
```

- Bu şekilde bir hata ile karşılaşılması da muhtemel. Bu durumda ya internet bağlantısı kaynaklı bir sorun yaşanıyordur ya da gitconfig ayarlarında sorun olabilir.

  - postBuffer değeri, git http protokolünü kullanarak dosya indirmek için kullanılan maksimum bellek boyutudur. *postBuffer* değeri yeterince yüksek değilse büyük boyutlu dosyaları clone ile almaya çalıştığımızda bu şekilde bir hata ile karşılaşma ihtimalimiz mevcut.

  - Bunu gidermek için değeri yüksek seçmek gerekiyor

    ```bash
    git config --global http.postBuffer 1073741824
    
    ```

    komutu kullanarak clone işlemini 1G boyutuna kadar izin vermesi için düzenlenebilir. 

#### 

